import java.util.Properties;
import java.util.Scanner;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;

 class Item {

	String name;
	float price;
	int quantity;
	String type;
	
	Item(String name, float price, int quantity, String type) {
		this.name = name;
		this.price = price;
		this.quantity = quantity;
		this.type = type;
	}

	void calculateTax(String type,float price,int quantity)
	{
		float tax=(float)12.5*price/100;
		float newprice=0;
		switch(type)
		{
			case "RAW":{ 
						newprice=quantity*(price+tax);
						break;
						}
		
			case "MANUFACTURED": {
						tax+=(float)2*(price + tax)/100;
						newprice=quantity*(price+tax);
						break;
						}
			
			case "IMPORTED": {
						tax=(float)10*price/100;
						newprice=quantity*(price+tax);
						
						if(newprice<=100)
							newprice+=5;
				
						else if(newprice>100 && newprice<=200)
							newprice+=10;
				
						else
							newprice+=20;
						
						break;
					}
			
			default:System.out.println("Invalid value");
		}
		
		System.out.println("Product tax per item: "+ tax);
		System.out.println("Product final price as per quantity: "+ newprice);
	}
	
	void getValues()
	{
		System.out.println("Product name: "+ name);
		System.out.println("Product price before tax: "+ price);
		System.out.println("Product quantity: "+ quantity);
		System.out.println("Product type: "+ type);
	}
	
	public static void main(String args[])throws Exception
	{
		Scanner sc=new Scanner(System.in);
		
		Options options = new Options();

	    Option propertyOption   = Option.builder()
	         .longOpt("1")
	         .argName("property=value" ) 
	         .hasArgs()
	         .valueSeparator()
	         .numberOfArgs(2)
	         .desc("use value for given properties" )
	         .build();

	      options.addOption(propertyOption);

	      CommandLineParser parser = new DefaultParser();
	      CommandLine cmd = parser.parse( options, args);
		
	      if(cmd.hasOption("1")) {
	    	  
	    	  try {
	    	  Properties properties = cmd.getOptionProperties("1");
	    	  String name=properties.getProperty("name");
	    	  float price=Float.parseFloat(properties.getProperty("price"));
	    	  int quantity=Integer.parseInt(properties.getProperty("quantity"));
	    	  String type=properties.getProperty("type");
		
	    	  Item p=new Item(name,price,quantity,type);

	    	  System.out.println("** Final product details are:- ** \n");

	    	  p.getValues();
	    	  p.calculateTax(type,price,quantity);
	    	  }
	    	  
	    	  catch(Exception e)
	    	  {
	    		  System.out.println("Exception is:");
	    		  e.printStackTrace();
	    	  }
	      }
	      System.out.println("\nDo you want to add more items \n"+
	    		  				"Press Y if yes or N if not");
		
	      char opt=sc.next().charAt(0);
	      sc.nextLine();
		

	      while(opt=='Y' || opt=='y'){
				
			try {
					System.out.print("Enter Product name:");
					String name=sc.nextLine();
	
					System.out.println("Enter Product price:");
					float price=sc.nextFloat();
	
					System.out.println("Enter Product quantity:");
					int quantity=sc.nextInt();
	
					System.out.println("Enter Product type: \n**Enter 1 for RAW**" +
											"\n**Enter 2 for MANUFACTURED**"+ 
												"\n**Enter 3 for IMPORTED** ");
					String type=sc.next();
					Item p=new Item(name,price,quantity,type);
		
					System.out.println("\n** Final product details are:- ** \n");
		
					p.getValues();
					p.calculateTax(type, price,quantity);
				}
		
			catch(Exception e)
				{	
					System.out.println("Exception is: ");
					e.printStackTrace();
				}

			System.out.println("\nDo you want to add more items \n"+
									"Press Y if yes or N if not");
			opt=sc.next().charAt(0);
			sc.nextLine();
		
		}
		sc.close();
	}
}
